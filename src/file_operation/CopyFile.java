package file_operation;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class CopyFile {
	public static void main(String args[]) throws IOException {
		System.out.println("Enter File Path");
		Scanner sc = new Scanner(System.in);
		String filePath = sc.nextLine();
		File file = new File(filePath);
		FileReader fr = new FileReader(file);
		BufferedReader br = new BufferedReader(fr);
		String newFilePath = "C:\\Users\\SANKET\\eclipse-workspace\\CoreJava\\src\\file_operation\\copyfile";
		File newfile = new File(newFilePath);
		FileWriter fw = new FileWriter(newfile);
		BufferedWriter bw = new BufferedWriter(fw);
		String line;
		while ((line = br.readLine()) != null) {

			bw.write(line);
			bw.write("/n");

		}
		bw.close();
		fw.close();
		fr.close();
		br.close();

	}
}
