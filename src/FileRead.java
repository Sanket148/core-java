import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class FileRead {
	public static void main(String[] args) throws IOException {
		String FileNameWithPath = "C:\\Users\\Sanket\\eclipse-workspace\\FileOperation\\src\\pledge.txt";
		File TextFile = new File(FileNameWithPath);
		FileReader fr = new FileReader(TextFile);
		BufferedReader br = new BufferedReader(fr);
		String line = "";
		
		while(line!=null) {
		line = br.readLine();
		System.out.println(line);
		}
		
		br.close();

	}
}
