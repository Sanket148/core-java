package collection_framework;

public class ArrayMain {
	public static void main(String args[]) {
int[] s=new int[5];

s[0]=5;
s[1]=6;
s[2]=7;
s[3]=8;
s[4]=9;

System.out.println("sum="+(s[0]+s[4]));
System.out.println("*****Using While Loop*****");
int a=0;
while(a<5)
{
	System.out.println(s[a]);
	a=a+1;
}
System.out.println("****Using For Loop");
for(int i=0;i<5;i=i+1) {
	System.out.println(s[i]);

	
}

}
	}		

